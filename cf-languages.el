(setq cf-pl-gcc "10")
(setq cf-pl-g++ "1")
(setq cf-pl-g++11 "16")
(setq cf-pl-go "32")
(setq cf-pl-haskel "12")
(setq cf-pl-java-6 "5")
(setq cf-pl-java-7 "23")
(setq cf-pl-java-8 "36")
(setq cf-pl-fpc "4")
(setq cf-pl-perl "13")
(setq cf-pl-php "6")
(setq cf-pl-python-2 "7")
(setq cf-pl-python-3 "31")
(setq cf-pl-ruby "8")
(setq cf-pl-scala "20")
(setq cf-pl-js "34")

;; <select name="programTypeId"">
;;   <option value="10">GNU GCC 4.9.2</option>
;;   <option value="1">GNU G++ 4.9.2</option>
;;   <option value="16">GNU G++11 4.9.2</option>
;;   <option value="2">Microsoft Visual C++ 2010</option>
;;   <option value="9">C# Mono 2.10</option>
;;   <option value="29">MS C# .NET 4</option>
;;   <option value="28">D DMD32 Compiler v2</option>
;;   <option value="32">Go 1.2</option>
;;   <option value="12">Haskell GHC 7.6</option>
;;   <option value="5">Java 6</option>
;;   <option value="23">Java 7</option>
;;   <option value="36">Java 8</option>
;;   <option value="19">OCaml 4</option>
;;   <option value="3">Delphi 7</option>
;;   <option value="4">Free Pascal 2</option>
;;   <option value="13">Perl 5.12</option>
;;   <option value="6">PHP 5.3</option>
;;   <option value="7">Python 2.7</option>
;;   <option value="31">Python 3.4</option>
;;   <option value="8">Ruby 2</option>
;;   <option value="20">Scala 2.11</option>
;;   <option value="34">JavaScript V8 3</option>
;; </select>